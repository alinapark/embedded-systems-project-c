# Embedded Systems Project C++

The project's goal was to create an IMU sensor 3D orientation visualization using QT GUI, as a client-side of the system, and BeagleBone, as a server-side.

The Madgwick fusion algorithm was used to improve the accuracy of the orientation data estimation. The input sensor
data was processed with quaternion estimation and gradient descent algorithm.